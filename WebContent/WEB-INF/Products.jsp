<%@page import="com.models.ProductModel"%>
<%@page import="java.util.ArrayList"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<head>
		<link rel="stylesheet" href="styles/home.css">
	</head>
	<body>
		<%@include file="components/header.jsp" %>
		
		<div>
			<h1>List of products</h1>
			<table width="100%" border='1'>
				<thead>
					<tr>
						<td>ID:</td>
						<td>Name:</td>
						<td>Price:</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${ productList }" var="product" varStatus="status">
						<tr>
							<td><c:out value="${ product.getID() }" /></td>
							<td><c:out value="${ product.getName() }" /></td>
							<td><c:out value="${ product.getPrice() }" /></td>
							
						</tr>
						<c:if test="${not empty special_product}">
						<tr>
							<td><c:out value="${ special_product.getID() }" /></td>
							<td><c:out value="${ special_product.getName() }" /></td>
							<td><c:out value="${ special_product.getPrice() }" /></td>
						</tr>
						</c:if>
					</c:forEach>
				</tbody>
			</table>
		</div>
		
		<form method="POST" action="products" style="margin:10px;dispaly:flex;flex-direction:column;">
		<input type="number" placeholder="id" name="p_id" >
		<input type="text" placeholder="name" name="p_name" >
		<input type="number" placeholder="price" name="p_price" >
		<button type="submit">Ajouter</button>
		</form>
		
		
		
		
	</body>
</html>