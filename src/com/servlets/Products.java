package com.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.ProductModel;

/**
 * Servlet implementation class Products
 */
@WebServlet("/Products")
public class Products extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Products() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ArrayList<ProductModel> products = new ArrayList<ProductModel>();
		for(int i = 0; i<10; i++) {
			ProductModel prd = new ProductModel();
			prd.setID(i);
			prd.setName("produit " + i);
			prd.setPrice(i);
			
			products.add(prd);
		}
		request.setAttribute("productList", products);
		request.setAttribute("productSize", products.size());
		
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/Products.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ProductModel product = new ProductModel();
		try {
		
		
		product.setID(Integer.parseInt(request.getParameter("p_id")))
		.setName(request.getParameter("p_name"))
		.setPrice(Float.parseFloat(request.getParameter("p_price")));
		}catch(Exception e) {
			
		}
		request.setAttribute("special_product", product);
		
		this.doGet(request, response);
	}

}
