package com.interfaces;

public interface IProducts {
	public float getPrice();
	public String getName();
	public int getID();
}
