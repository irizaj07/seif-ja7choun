package com.models;

import com.interfaces.IProducts;

public class ProductModel implements IProducts {

	private String name;
	private float price;
	private int id;
	
	
	public ProductModel setPrice(float price) {
		this.price = price;
		return this;
	}
	
	public ProductModel setID(int id) {
		this.id = id;
		return this;
	}
	
	public ProductModel setName(String name) {
		this.name = name;
		return this;
	}
	
	@Override
	public float getPrice() {
		// TODO Auto-generated method stub
		return this.price;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return this.name;
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return this.id;
	}

}
